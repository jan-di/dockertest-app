#!/bin/sh
set -eux

docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
docker push --quiet $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
